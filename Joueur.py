# -*- coding: utf-8 -*-

from ObjetAffichable import ObjetAffichable

class Joueur(ObjetAffichable) :


    def __init__(self, coordonees, cheminImage, animation_numColTotal, animation_numLinTotal, animation_vitesse) :
        self.vitesseX = 0
        self.vitesseY = 0
        self.onGround = False
        super().__init__(coordonees, cheminImage, animation_numColTotal, animation_numLinTotal, animation_vitesse)


    def update(self, event, tableauPlateforme) :
        self.majVitesse(event)
        self.testCollisions(tableauPlateforme)
        self.updateAnimation()


    def majVitesse(self, event):
        # On augmente la vitesse X (gauche/droite) petit à petit pour donner un effect d'accélération, avec un maximum à 5
        if event.right :
            self.vitesseX = min((self.vitesseX + 0.3), 5)
            self.changeAnimation(0)
        elif event.left :
            self.vitesseX = max((self.vitesseX - 0.3), -5)
            self.changeAnimation(1)
        # on ralentit petit à petit quand on lache les directions
        else :
            if self.vitesseX > 0 :
                self.vitesseX = max((self.vitesseX - 0.3), 0)
                self.changeAnimation(2)
            elif self.vitesseX < 0 :
                self.vitesseX = min((self.vitesseX + 0.3), 0)
                self.changeAnimation(3)

        # Quand on saute, on passe directement la vitesse Y (haut/bas) à -10, la gravité fera augmenter cette valeur petit à petit jusqu'à retomber sur le sol
        if event.up and self.onGround :
            self.vitesseY = -10

        # Action de la gravité : on augmente la vitesse Y petit à petit pour faire tomber le perso (avec une vitesse de chute max de 30)
        if not self.onGround:
            self.vitesseY = min((self.vitesseY + 0.3), 30)


    def testCollisions(self, tableauPlateforme) :
        # On teste d'abord les collisions gauche/droite, si collidelist vaut -1 c'est qu'on a pas de collisions et qu'on peut donc se déplacer
        collideHorizontal = self.move(self.vitesseX, 0).collidelist(tableauPlateforme)
        if collideHorizontal == -1 :
            self.move_ip(self.vitesseX, 0)
        # Sinon si on a une collision vers la droite, on colle le joueur au bloc de droite
        elif self.vitesseX > 0:
            self.right = tableauPlateforme[collideHorizontal].left
        # Sinon c'est qu'on a une collision vers la gauche, donc pareil on colle le joueur au bloc de gauche
        elif self.vitesseX < 0:
            self.left = tableauPlateforme[collideHorizontal].right

        # Même chose pour les collisions haut/bas
        collideVertical = self.move(0, self.vitesseY).collidelist(tableauPlateforme)
        if collideVertical == -1:
            self.move_ip(0, self.vitesseY)
            self.onGround = False
        elif self.vitesseY > 0:
            self.bottom = tableauPlateforme[collideVertical].top
            # Si on est dans ce cas ca veut dire qu'on a une collision vers le bas, qu'on est donc sur le sol.
            self.onGround = True
        elif self.vitesseY < 0:
            self.top = tableauPlateforme[collideVertical].bottom
            # Si on a une collision vers le haut il faut qu'on rebondisse direct sans être collé au plafond
            self.vitesseY = 0