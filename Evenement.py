# -*- coding: utf-8 -*-

import sys, pygame

class Evenement :
    def __init__(self) :
        self.right = False
        self.left = False
        self.up = False
        self.down = False

    def update(self) :
        # On récupère les évenements qui nous intéressent (appui sur une touche, click de la souris ...)
        for event in pygame.event.get() :
            if event.type == pygame.QUIT :
                sys.exit()
            elif event.type == pygame.KEYDOWN :
                if event.key == pygame.K_LEFT :
                    self.left = True
                elif event.key == pygame.K_RIGHT :
                    self.right = True
                elif event.key == pygame.K_UP :
                    self.up = True
                elif event.key == pygame.K_DOWN :
                    self.down = True
            elif event.type == pygame.KEYUP :
                if event.key == pygame.K_LEFT :
                    self.left = False
                elif event.key == pygame.K_RIGHT :
                    self.right = False
                elif event.key == pygame.K_UP :
                    self.up = False
                elif event.key == pygame.K_DOWN :
                    self.down = False
