# -*- coding: utf-8 -*-

from Joueur import Joueur
from ObjetAffichable import ObjetAffichable
from ImageFond import ImageFond
from Camera import Camera

class Niveau :
    def __init__(self, screen, windowsWidth, windowsHeight) :
        self.screen = screen
        self.windowsWidth = windowsWidth
        self.windowsHeight = windowsHeight
        self.niveauEnCours = 0
        self.levelList = [
            [
                "_____________________________________________________________________________________________________",
                "_                                                                                                   _",
                "_                                                                                                   _",
                "_                                                                                                   _",
                "_                                                                                                   _",
                "_                                                                                            X      _",
                "_                                                                                            _      _",
                "_                                                                                       _           _",
                "_                                                                                 __                _",
                "_                        _____                                              ___                     _",
                "_                                                                   ____                            _",
                "_                _____                                    _____                                     _",
                "_                                                                                                   _",
                "_        _____                                 ______                                               _",
                "_                                                                                                   _",
                "_ P                                                                                                 _",
                "__________________________             ______________________________________________________________"
            ],
            [
                "_____________________________________________________________________________________________________",
                "_                                                                                                   _",
                "_    X                                                                                              _",
                "_                                                                                                   _",
                "_                                                                    __                             _",
                "_                                                                                                   _",
                "_       _        _        _        _        _        _        _                                     _",
                "_                                                                          _                        _",
                "_                                                                                                   _",
                "_                                                                                                   _",
                "_                                                                        _____                      _",
                "_                                                           _____                                   _",
                "_                                              _____                                                _",
                "_                                 _____                                                             _",
                "_                    _____                                                                          _",
                "_   P                                                                                               _",
                "________________                                                                                    _"
            ]
        ]
        self.load(self.niveauEnCours)


    def load(self, index) :
        # on fait en sorte que index ne soit pas trop grand, par ex load(5) quand on a que 2 niveaux va loader le niveau (5%2) c'est à dire 1
        index = index % len(self.levelList)

        self.niveauEnCours = index
        self.level = self.levelList[index]

        self.numberOfCol = len(self.level[0])
        self.numberOfLin = len(self.level)

        # On va faire en sorte que les cases soient carrées, et on va se baser sur la hauteur de la fenetre pour définir
        # la taille d'une case (car notre perso va scroller horizontalement vers la droite). Si à la place on veut faire un scroll
        # vertical, il faut se baser sur la largeur à la place (cellHeight = cellWidth = width / numberOfCol)
        # Si on veut un scroll à la fois horizontal et vertical, prendre une valeur définie à la main.
        self.cellHeight = self.cellWidth = self.windowsHeight / self.numberOfLin

        # On transforme les plateformes du niveau en tableau de pygame.Rect
        self.tableauPlateforme = []
        for x in range(self.numberOfCol) :
            for y in range(self.numberOfLin) :
                coordonnees = (x*self.cellWidth, y*self.cellHeight, self.cellWidth+1, self.cellHeight+1)
                if self.level[y][x] == "_" :
                    self.tableauPlateforme.append(ObjetAffichable(coordonnees, "images/platform_" + str(self.niveauEnCours) + ".png"))
                elif self.level[y][x] == "P" :
                    self.perso = Joueur(coordonnees, "images/perso.png", 10, 4, 8)
                elif self.level[y][x] == "X" :
                    self.sortie = ObjetAffichable(coordonnees, "images/sortie.png")

        self.background = ImageFond((0, 0, self.windowsWidth, self.windowsHeight), "images/laitue.jpg")

        # Création de la camera bornée sur les limites du niveau
        xmin = 0
        xmax = ((self.cellWidth * self.numberOfCol) - self.windowsWidth)
        ymin = 0
        ymax = ((self.cellHeight * self.numberOfLin) - self.windowsHeight) # vaut 0 dans le cas d'un scroll horizontal
        self.camera = Camera(xmin, xmax, ymin, ymax, self.windowsWidth, self.windowsHeight)


    def loadNext(self) :
        self.load(self.niveauEnCours + 1)


    def reload(self) :
        self.load(self.niveauEnCours)


    def update(self, event) :
        self.perso.update(event, self.tableauPlateforme)
        self.camera.update(self.perso)


    def affiche(self) :
        self.background.affiche(self.screen, self.camera)

        for plateforme in self.tableauPlateforme :
            plateforme.affiche(self.screen, self.camera)

        self.perso.affiche(self.screen, self.camera)
        self.sortie.affiche(self.screen, self.camera)


    def testGagne(self) :
        return self.perso.colliderect(self.sortie)


    def testPerdu(self) :
        persoY = self.perso.y - self.camera.scrollY
        return persoY > self.windowsHeight or persoY  < 0
