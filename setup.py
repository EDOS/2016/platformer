# Création d'un fichier exe avec cx_freeze pour partager le jeu plus facilement

# Pour l'installer :
# pip install cx_freeze

# Pour l'utiliser :
# python3 setup.py build

from cx_Freeze import setup, Executable

setup(
    name = "main",
    version = "0.1",
    description = "Mon super jeu de plateforme",
    executables = [Executable("main.py")],
)
