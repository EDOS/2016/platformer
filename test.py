import sys, pygame
from pygame import *

size = width, height = 800, 600

pygame.init()
screen = pygame.display.set_mode(size)

level = [
    "_____________________________________________________________________________________________________",
    "_                                                                                                   _",
    "_                                                                                                   _",
    "_                                                                                                   _",
    "_                                                                                                   _",
    "_                                                                                            X      _",
    "_                                                                                                   _",
    "_                                                                                       _           _",
    "_                                                                                 __                _",
    "_                        _____                                              ___                     _",
    "_                                                                   ____                            _",
    "_                _____                                    _____                                     _",
    "_                                                                                                   _",
    "_        _____                                 ______                                               _",
    "_                                                                                                   _",
    "_ P                                                                                                 _",
    "__________________________             ______________________________________________________________"
]

numberOfCol = len(level[0])
numberOfLin = len(level)

# On va faire en sorte que les cases soient carrées, et on va se baser sur la hauteur de la fenetre pour définir
# la taille d'une case (car notre perso va scroller horizontalement vers la droite). Si à la place on veut faire un scroll 
# vertical, il faut se baser sur la largeur à la place (cellHeight = cellWidth = width / numberOfCol)
# Si on veut un scroll à la fois horizontal et vertical, prendre une valeur définie à la main.
cellHeight = cellWidth = height / numberOfLin

up = down = right = left = False

# Stockage des différents types de choses affichées à l'écran dans un pygame.rect
perso = sortie = plateforme = pygame.Rect(0, 0, cellWidth+1, cellHeight+1)

persoVelX = 0
persoVelY = 0

onGround = False

scrollScreen = 0

# On transforme les plateformes du niveau en tableau de pygame.Rect
tableauPlateforme = []
for x in range(numberOfCol) :
    for y in range(numberOfLin) :
        plateforme = pygame.Rect(x*cellWidth, y*cellHeight, cellWidth+1, cellHeight+1)
        if level[y][x] == "_" :
            tableauPlateforme.append(plateforme)
        elif level[y][x] == "P" :
            perso = plateforme
        elif level[y][x] == "X" :
            sortie = plateforme



clock = pygame.time.Clock()
while 1:
    # Avec clock.tick(100) chaque passage dans la boucle représente une frame affichée, et on affiche 100 frame par seconde
    clock.tick(100)



    # On récupère les évenements qui nous intéressent (appui sur une touche, click de la souris ...)
    for event in pygame.event.get():
        if event.type == pygame.QUIT :
            sys.exit()
        elif event.type == pygame.KEYDOWN :
            if event.key == pygame.K_LEFT :
                left = True
            elif event.key == pygame.K_RIGHT :
                right = True
            elif event.key == pygame.K_UP :
                up = True
            elif event.key == pygame.K_DOWN :
                down = True
        elif event.type == pygame.KEYUP :
            if event.key == pygame.K_LEFT :
                left = False
            elif event.key == pygame.K_RIGHT :
                right = False
            elif event.key == pygame.K_UP :
                up = False
            elif event.key == pygame.K_DOWN :
                down = False



    # On augmente la vélocité X (gauche/droite) petit à petit pour donner un effect d'accélération, avec un maximum à 5
    if right :
        persoVelX = min((persoVelX + 0.3), 5)
    elif left :
        persoVelX = max((persoVelX - 0.3), -5)
    # on ralentit petit à petit quand on lache les directions
    else :
        if persoVelX > 0 :
            persoVelX -= 0.3
        if persoVelX < 0 :
            persoVelX += 0.3

    # Quand on saute, on passe directement la vélocité Y (haut/bas) à -10, la gravité fera augmenter cette valeur petit à petit jusqu'à retomber sur le sol
    if up and onGround :
        persoVelY = -10

    # Action de la gravité : on augmente la vélocité Y petit à petit pour faire tomber le perso (avec une vitesse de chute max de 30)
    if not(onGround) :
       persoVelY = min((persoVelY + 0.3), 30)



    # On teste d'abord les collisions gauche/droite, si collidelist vaut -1 c'est qu'on a pas de collisions et qu'on peut donc se déplacer
    collideHorizontal = perso.move(persoVelX, 0).collidelist(tableauPlateforme)
    if collideHorizontal == -1 :
        perso = perso.move(persoVelX, 0)
        # Si le perso est positionné dans la moitié droite de l'écran et qu'on va a droite, on scrolle tout le niveau vers la gauche
        if persoVelX > 0 and (perso.x - scrollScreen) > (0.55 * width) :
            scrollScreen = min((scrollScreen + persoVelX), ((cellWidth * numberOfCol) - width))
        # Même chose quand le perso va vers la gauche
        if persoVelX < 0 and (perso.x - scrollScreen) < (0.45 * width) :
            scrollScreen = max((scrollScreen + persoVelX), 0)
    # Sinon si on a une collision vers la droite, on colle le joueur au bloc de droite
    elif persoVelX > 0 :
        perso.right = tableauPlateforme[collideHorizontal].left
    # Sinon c'est qu'on a une collision vers la gauche, donc pareil on colle le joueur au bloc de gauche
    elif persoVelX < 0 :
        perso.left = tableauPlateforme[collideHorizontal].right

    # Même chose pour les collisions haut/bas
    collideVertical = perso.move(0, persoVelY).collidelist(tableauPlateforme)
    if collideVertical == -1 :
        perso = perso.move(0, persoVelY)
        onGround = False
    elif persoVelY > 0 :
        perso.bottom = tableauPlateforme[collideVertical].top
        # Si on est dans ce cas ca veut dire qu'on a une collision vers le bas, qu'on est donc sur le sol.
        onGround = True
    elif persoVelY < 0 :
        perso.top = tableauPlateforme[collideVertical].bottom 
        # Si on a une collision vers le haut il faut qu'on rebondisse direct sans être collé au plafond
        persoVelY = 0



    # On regarde si le perso a touché la sortie
    if perso.colliderect(sortie) :
        print("Gagné !")
        sys.exit()



    # On dessine le fond
    pygame.draw.rect(screen, (0, 0, 0), (0, 0, width, height))

    # On dessine les plateforme
    for plateforme in tableauPlateforme :
        pygame.draw.rect(screen, (0, 200, 0), plateforme.move(-scrollScreen,0))

    # On dessine le perso
    pygame.draw.rect(screen, (0, 150, 150), perso.move(-scrollScreen,0))

    # On dessine la sortie
    pygame.draw.rect(screen, (200, 0, 200), sortie.move(-scrollScreen,0))



    pygame.display.update()

