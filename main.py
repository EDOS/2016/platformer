# -*- coding: utf-8 -*-

import pygame
from Niveau import Niveau
from Evenement import Evenement

size = windowsWidth, windowsHeight = 800, 600

pygame.init()
screen = pygame.display.set_mode(size)

niveau = Niveau(screen, windowsWidth, windowsHeight)
event = Evenement()

clock = pygame.time.Clock()
while 1:
    # Avec clock.tick(100) chaque passage dans la boucle représente une frame affichée, et on affiche 100 frame par seconde
    clock.tick(100)

    event.update()
    niveau.update(event)
    niveau.affiche()

    if niveau.testGagne() :
        niveau.loadNext()

    if niveau.testPerdu() :
        niveau.reload()

    pygame.display.update()
