# -*- coding: utf-8 -*-

import pygame

class ObjetAffichable(pygame.Rect) :

    def __init__(self, coordonees, cheminImage, animation_numColTotal = None, animation_numLinTotal = None, animation_vitesse = None) :
        super().__init__(coordonees)

        # Animation
        if animation_numColTotal != None and animation_numLinTotal != None and animation_vitesse != None :
            self.animated = True
            self.animation_numColTotal = animation_numColTotal
            self.animation_numLinTotal = animation_numLinTotal
            self.animation_vitesse = animation_vitesse
            self.animation_ligne = 0
            self.animation_colonne = 0
            self.animation_tick = 0
        else :
            self.animated = False

        self.image = pygame.image.load(cheminImage).convert_alpha()
        self.redimensionner()


    def updateAnimation(self) :
        # Si on est pas animé, on sort de la fonction
        if not self.animated :
          return

        self.animation_tick += 1
        if self.animation_tick == self.animation_vitesse :
            self.animation_tick = 0
            self.animation_colonne = (self.animation_colonne + 1) % self.animation_numColTotal


    def changeAnimation(self, animation_ligne) :
        self.animation_ligne = animation_ligne


    def redimensionner(self) :
        if self.animated :
           self.image = pygame.transform.scale(self.image, (self.width * self.animation_numColTotal, self.height * self.animation_numLinTotal))
        else :
           self.image = pygame.transform.scale(self.image, (self.width, self.height))


    def affiche(self, screen, camera) :
        if self.animated :
            animationRect = pygame.Rect(self.animation_colonne * self.width, self.animation_ligne * self.height, self.width, self.height)
            screen.blit(self.image, self.move(camera.apply()), animationRect)
        else :
            screen.blit(self.image, self.move(camera.apply()))

