# -*- coding: utf-8 -*-


class Camera :


    def __init__(self, xmin, xmax, ymin, ymax, windowsWidth, windowsHeight) :
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.windowsWidth = windowsWidth
        self.windowsHeight = windowsHeight

        self.scrollX = 0
        self.scrollY = 0


    def update(self, perso) :
        moitieDroite = (0.55 * self.windowsWidth)
        moitieGauche = (0.45 * self.windowsWidth)
        moitieHaute = (0.55 * self.windowsHeight)
        moitieBasse = (0.45 * self.windowsHeight)

        # Si le perso est positionné dans la moitié droite ou dans la moitié gauche de l'écran, on positionne la camera sur le perso (sans dépasser xmin/xmax)
        if (perso.x - self.scrollX) > moitieDroite :
            self.scrollX = min(perso.x - moitieDroite, self.xmax)
        elif (perso.x - self.scrollX) < moitieGauche :
            self.scrollX = max(perso.x - moitieGauche, self.xmin)

        # Même chose pour le scroll haut/bas
        if (perso.y - self.scrollY) > moitieHaute :
            self.scrollY = min(perso.y - moitieHaute, self.ymax)
        elif (perso.y - self.scrollY) < moitieBasse :
            self.scrollY = max(perso.y - moitieBasse, self.ymin)


    def apply(self) :
        # On retourne des valeurs négatives car on veut que le scroll soit fait à l'inverse du déplacement du joueur : Si il va vers la droite, on scrolle le niveau vers la gauche.
        return (-self.scrollX, -self.scrollY)