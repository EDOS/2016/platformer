# -*- coding: utf-8 -*-

import pygame
from ObjetAffichable import ObjetAffichable

class ImageFond(ObjetAffichable) :

 
    def redimensionner(self) :
        # On redimensionne l'image en se basant sur la hauteur sans casser son ratio (pour pas déformer le fond)
        imageRect = self.image.get_rect()
        imageRatio = imageRect.height / imageRect.width
        self.width = int(self.width * imageRatio)

        self.image = pygame.transform.scale(self.image, (self.width, self.height))
 
 
    def affiche(self, screen, camera) :
        # On répète le fond le long de l'axe x
        scroll = self.move(camera.apply())
        screen.blit(self.image, scroll)

        while scroll.right < screen.get_rect().right :
            scroll.move_ip(self.width, 0)
            screen.blit(self.image, scroll)



